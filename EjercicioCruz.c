#include "raylib.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1980;
    const int screenHeight = 1080;
    int Line1X = screenWidth/2;
    int Line1Y = screenHeight;
    int Line1XE = screenWidth/2;
    int Line1YE = 0;
    int Line2X = screenWidth;
    int Line2Y = screenHeight/2;
    int Line2XE = 0;
    int Line2YE = screenHeight/2;
    int x = 0;
    int y = 0;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        if (IsKeyDown(KEY_RIGHT)){
            Line1X += 2;
            Line1XE += 2;
        }
        if (IsKeyDown(KEY_LEFT)){
            Line1X -= 2;
            Line1XE -= 2;
        }
        if (IsKeyDown(KEY_UP)){
            Line2Y -= 2;
            Line2YE -= 2; 
        }
        if (IsKeyDown(KEY_DOWN)){
            Line2Y += 2;
            Line2YE += 2;
        }
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(SKYBLUE);
            
            DrawText("1A, Roger Font", x, y, 40, BLACK);
        
            DrawLine(Line1X, Line1Y, Line1XE, Line1YE, BLACK);
            DrawLine(Line2X, Line2Y, Line2XE, Line2YE, BLACK);
            
         
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}