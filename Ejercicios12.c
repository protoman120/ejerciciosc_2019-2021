#include <stdio.h>
#include <stdlib.h>

int main()
{
int contador = 1;
int temvalue;
int totalsum;
int totalproduct = 1;

//Uso el while para restringir el numero de veces que se realiza el bucle.
while (contador < 10)
{
    printf("Enter value %d\n", contador);
    scanf("%d", &temvalue);
    getchar();
    totalsum += temvalue;
    totalproduct = temvalue * totalproduct;
    //Una vez realizados los calculos, la variable contador augmenta en 1, para parar a las 10 opraciones.
    contador++;
}

    printf("The sum is %d\n", totalsum);
    printf("The product is %d\n", totalproduct);
    
    getchar();
    return 0;
    
}
