#include <stdio.h>
#include <stdlib.h>

int main()
{
    int values;
    int numbers;
    time_t t;
    
    //Uso la variable "numbers" para limitar el numero de resultados que queremos.
    numbers = 5;
    
    //Creo un "srand" que sirve para generar valores aleatorios, en este caso seran enteros.
    srand((unsigned) time(&t));
    
    //Mientras la variable "values" sea menos que "numbers", se ira ejecutando la operacion, cada vez que termina, el valor de "values" sube en uno, por lo que terminarà al generar 5 valores.
    for (values = 0 ; values < numbers ; values++){
    
    //El % 9 sirve para limitar el valor maximo de los numeros generados.
    printf("%d\n", rand() % 9);
    }
    getchar();
    return 0;
}