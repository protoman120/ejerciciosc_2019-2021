#include <stdio.h>
#include <stdlib.h>

int main()
{
int seconds;
float minutes;
float hours;
float days;

//Pedimos un valor entero en segundos.
printf("Enter a value in seconds\n");
scanf("%d", &seconds);
getchar();

//Usamos floats para los minutos, horas y dias, por si se usan valores demasiado pequeños para representar con enteros.
minutes = seconds / 60;
hours = minutes / 60;
days = hours / 60;
printf("%d seconds is %f minutes, %f hours and %f days\n", seconds, minutes, hours, days);

getchar();
return 0;
}