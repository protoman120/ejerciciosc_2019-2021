#include <stdio.h>
#include <stdlib.h>

int isnegative(int number);

int main(){

int number;
int pow;
int res;

//Pido un numero al usuario.
printf("Introduce a number...\n");
scanf("%d", &number);
getchar();

//Pide el valor de la potencia.
printf("Introduce the power you wish to apply to the number\n");
scanf("%d", &pow);
getchar();

//Se llama a la funcion "raisetopower" para obtener el resultado de la operacon y mostrarlo en la pantalla.
res = raisetopower(number, pow);

printf("The result is %d", res);

getchar();
return 0;
}

int raisetopower(int value, int power){

int result = 1;
int i;
//Se calcula el resultado del valor con la potencia deseada, usando la variable i como un contador. Una vez resulto podra ser interpretado por la funcion principal.
for (i = 0 ; i < power ; i++)
{
result *= value;
}
return result;
}
