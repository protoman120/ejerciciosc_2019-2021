#include <stdio.h>
#include <stdlib.h>

int main()
{
float radius;

//pedimos el valor del radio.
printf ("Enter the radius value\n");
scanf("%f", &radius);
getchar();

//Ya que para encontrar el diametro, circumferencia y area del circulo se necesita el numero pi, creo una variable con su valor aproximado, ya que tiene infinitos decimales.
float pi = (3.14);

//Diametro = radio * 2.
float diameter = radius * 2;
printf("The diameter is %f\n", diameter);

//Circumferencia = 2 * pi * radio.
float circumference = 2 * pi * radius;
printf("The circumference is %f\n", circumference);

//Area = pi * radio ^ 2 (radio * radio).
float area = pi * radius * radius;
printf("The area is %f\n", area);

getchar();
return 0;
}