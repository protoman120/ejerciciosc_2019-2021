#include <stdio.h>
#include <stdlib.h>

int main()
{
int temvalue;
int totalsum = 0;

//Usamos la variable "temvalue" para tener un valor que cambiara constantemente con cada operacion, tambien comprueba que el numero sea positivo, y por tanto, no se cierre el programa.
while (temvalue >= 0)
{
    printf("Introduce a value\n");
    scanf("%i", &temvalue);
    getchar();
    totalsum += temvalue;
    printf("The total sum of the numbers introduced is %d\n", totalsum);
    printf("Press enter to continue entering numbers...\n");
    getchar();
}

//Al introducir un numero negativo, el programa termina.
printf("A negative value has been entered, ending program...");
getchar();
return 0;
}