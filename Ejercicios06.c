#include <stdio.h>
#include <stdlib.h>

int main()
{
float centimeters;
float meters;
float kilometers;

//Pedimos un valor en centimentros, usamos el float para poder representar los decimas en caso que se de un valor pequeño.
printf ("Enter the value in centimiters\n");
scanf("%f", &centimeters);
getchar();

//Metros = centimetros / 100.
meters = centimeters / 100;
printf("%f centimeters is %f meters", centimeters, meters);

//Kilometros = centimetros / 1000.
kilometers = centimeters / 1000;
printf("%f centimeters is %f kilometers", centimeters, kilometers);

getchar();
return 0;
}