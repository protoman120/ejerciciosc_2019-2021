#include <stdio.h>
#include <stdlib.h>

int main()
{
int angle1;
int angle2;
int angle3;

// Pedimos la introdcucción del perimer angulo.
printf("Enter the first angle\n");
scanf("%d", &angle1);
getchar();

// Nos aseguramos que el angulo no sea negativo o superior a los 180 grados, dejando de ser un triangulo.
while (angle1 < 0 || angle1 > 180)
    {
        printf("The introduced value is either negative or bigger than 180, plese introduce another value\n");
        scanf("%d", &angle1);
        getchar();
    }

printf("Enter the second angle\n");
scanf("%d", &angle2);
getchar();
while (angle2 < 0 || angle2 > 180)
    {
        printf("The introduced value is either negative or bigger than 180, plese use another value\n");
        scanf("%d", &angle2);
        getchar();
    }

//Comprobacion final de si el resultado da mas de 180 grados o negativo.
angle3 = 180 - angle1 - angle2;
while (angle3 > 180 || angle3 < 0)
{
    printf("The total value is either negative or higher than 180 degrees, please reenter the values\n");
    getchar();
    return 0;
}
printf("The third angle is %d", angle3);

getchar();
return 0;
}