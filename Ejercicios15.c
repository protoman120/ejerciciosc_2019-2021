#include <stdio.h>

int number;
int binary;

int main()
{
do{
printf("Enter the number you wish to convert into binary\n");
scanf("%d", &number);
getchar();
}while (number < 0);

while (number > 0)
{
//Usamos el resto para encontrar el valor binario de ese numero
binary = number % 2;
number /= 2;
printf ("%d", binary);
}
printf("\n");

getchar();
return 0;
}
