#include <stdio.h>
#include <stdlib.h>

int main(){

//Uso floats para permitir el uso de decimales, exepto en la variable "operador", ya que solo servia para elegir que operacion queremos hacer, o cerrar el programa.
float numbers1;
float numbers2;
int operator;
float result;

do{

printf("Enter a number, it can have decimals\n");
scanf("%f", &numbers1);
getchar();

//Asigno un numero a cada tipo de operación, incluyendo una para cerrar el programa.
printf("Which operation you wish to make? Enter the corresponding number for the desired operation\n");
printf("- 1. Add (+)\n");
printf("- 2. Substract (-)\n");
printf("- 3. Multiply (*)\n");
printf("- 4. Divide (/)\n");
printf("- 5. End program\n");
scanf("%d", &operator);
getchar();

printf("Enter a second number, it can have decimals\n");
scanf("%f", &numbers2);
getchar();

//Uso varios "if" para las diferentes operaciones que realizara el programa.
if (operator == 1){
    result = numbers1 + numbers2;
    printf("%f + %f = %f\n", numbers1, numbers2, result);
    }
if (operator == 2){
    result = numbers1 - numbers2;
    printf("%f - %f = %f\n", numbers1, numbers2, result);
    }
if (operator == 3){
    result = numbers1 * numbers2;
    printf("%f * %f = %f\n", numbers1, numbers2, result);
    }
if (operator == 4){
    result = numbers1 / numbers2;
    printf("%f / %f = %f\n", numbers1, numbers2, result);
    }

//Pongo un "while" al final porque se tiene que repetir este proceso mientras no se introduza un valor especifico, en este caso "5".
}while (operator !=5);

//Si se introduce el valor para cerrar el programa, en este caso "5", el programa se cerrarà.
printf("Ending program...\n");
getchar();
return 0;
}
