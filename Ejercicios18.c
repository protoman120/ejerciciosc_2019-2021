#include <stdio.h>
#include <stdlib.h>

int isnegative(int number);

int main()
{
int number;
int finalresult;

//Pido un numero entero
printf("Introduce a number...\n");
scanf("%d", &number);
getchar();

//Llamo la funcion creada con la variable "finalresult", y dependiendo del valor que se le ha asignado se atribuye un resultado.
finalresult = isnegative(number);

if (finalresult == 1){
    printf("The introduced number is positive\n");
    }
if (finalresult == 2){
    printf("The introduced number is negative\n");
    }
if (finalresult == 3){
    printf("The introduced number is 0, so it's neither positive or negative\n");
    }
printf("Ending program...\n");
getchar();
return 0;
}

//Se crea una funcion para que el programa pueda determinar si el numero es positivo, negativo o 0, pare ello se atribuye, 1, 2 o 3 para que la funcion principal lo pueda entender.
int isnegative(int number){

int result;

if (number > 0){
    result = 1;
    }
if (number < 0){
    result = 2;
    }
if (number == 0){
    result = 3;
    }
return result;  
}
