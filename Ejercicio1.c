#include <stdlib.h>
#include <stdio.h>

int main()
{
   int numero1 = 5;
   int numero2 = 4;
   int numero3 = numero1 + numero2;

   printf("El primer numero es %d\n", numero1);
   printf("El segundo numero es %d\n", numero2);
   printf("El resultado es %d\n", numero3);
   
   getchar();
   
   return 0;
}