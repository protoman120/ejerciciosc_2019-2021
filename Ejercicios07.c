#include <stdio.h>
#include <stdlib.h>

int main()
{
float celsius;
float fahrenheit;

//Pedimos el valor en grados celsius, usamos float para admitir decimales por si se necesita.
printf("Enter the value in celsius degrees\n");
scanf("%f", &celsius);
getchar();

//Para lograr grados fahrenheit desde celsius, hacemos el siguiente calculo.
fahrenheit = 1.8f + celsius + 32;
printf("%f degrees celsius is %f degrees fahrenheit\n", celsius, fahrenheit);

getchar();
return 0;
}